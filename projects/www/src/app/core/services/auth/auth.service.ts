import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  readonly TOKEN_KEY = 'token-gestor-leads'

  constructor(private storageService: StorageService) {  }

  /*
  * Retorna o token de autenticacão.
  */
  public getToken(): string {
    // TODO -> Implementar localStorage.
    return this.storageService.getData(this.TOKEN_KEY);
  }

  /*
  * Retorna se o token está válido
  */
  public isAuthenticated(): boolean {
    const token = this.getToken();
    // TODO -> Implementar validacão com backend.
    return !!token;
  }

}
