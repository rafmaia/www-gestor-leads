import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  toastPayload = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  constructor() { }

  error(title: string, message: string) {
    Swal.fire(title, message, 'error');
  }

  success(title: string, message: string) {
    Swal.fire(title, message, 'success');
  }

  info(title: string, message: string) {
    Swal.fire(title, message, 'info');
  }

  toast(message: string) {
    this.toastPayload.fire({icon: 'success', title: message});
  }

  custom(payload: any) {
    Swal.fire(payload);
  }

}
