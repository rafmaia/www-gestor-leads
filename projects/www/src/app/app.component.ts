import { Component } from '@angular/core';
import { AlertService } from './core/services/alert/alert.service';

@Component({
  selector: 'pan-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'www';

  constructor(private alertService: AlertService) {}

  alert() {
    this.alertService.success('Teste', 'Teste Titulo');
  }
}
